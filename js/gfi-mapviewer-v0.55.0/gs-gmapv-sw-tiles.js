/*
 * Permet a gs-gmapv de mettre des tuiles dans le cache pour un accès hors-ligne
 * Le listener sur fetch permet simplement de retourner les requêtes si elles
 * sont présente en cache.
 * Le listener message permet d'écouter deux messages transmis par gs-gmapv
 * Le permier 'cacheRequest' permet de mettre en cache une tuile
 *   Ex de message : {
 *     type: 'cacheRequest',
 *     url: 'https://stamen-tiles-a.a.ssl.fastly.net/toner-lite/5/19/11.png',
 *     force: true // Permet de mettre en cache une tile déjà présente (upload)
 *   }
 * Le second 'clearRequest' permet de détruire le cache. Sa réponse est un
 * Boolean indiquant la réussite de la suppression
 *
 * Dans le cas des deux messages, un MessageChannel est requis pour la réponse
 */
(() => {
  let tileCacheName = 'gs-gmapv-cache-tiles' + (self.registration ? self.registration.scope : '')
  let cacheKeys = [] // Ensemble des requêtes présentes en cache

  self.addEventListener('install', event => {
    event.waitUntil(caches.delete(tileCacheName).then(() => self.skipWaiting()))
  })

  self.addEventListener('activate', event => {
    event.waitUntil(self.clients.claim())
  })

  // Si la requête est en cache, on la retourne
  self.addEventListener('fetch', event => {
    if (cacheKeys.includes(event.request.url)) {
      event.respondWith(caches.open(tileCacheName).then(cache => {
        return cache.match(event.request.url).then(response => {
          if (response) {
            return response
          }
          throw Error('The cached response that was expected is missing.')
        })
      }))
    }
  })

  // Permet d'acouter la réception du message du client
  self.addEventListener('message', event => {
    // On verifie que sa structure est correcte
    if (event.data && event.data.type) {
      // Dans le cas d'une demande de mise en cache
      if (event.data.type === 'cacheRequest') {
        return caches.open(tileCacheName).then(cache => {
          // On check s'il est déjà en cache
          let alreadyCached = cacheKeys.includes(event.data.url)

          // On ne le met en cache que s'il n'y est pas ou si c'est forcé
          if (!alreadyCached || event.data.force) {
            // Récupère la ressource en ligne
            return fetch(new Request(event.data.url)).then(response => {
              // Si la requête n'est pas valide
              if (!response.ok) {
                return 'error'
              }
              // La pousse dans le cache
              cache.put(event.data.url, response.clone())
              // Si elle n'etait pas présente, on l'ajoute dans cacheKeys
              if (!alreadyCached) {
                cacheKeys.push(event.data.url)
              }
              return 'cached'
            }).catch(() => 'error')
          }
          return 'alreadyCached'
        }).then(status => {
          // On retourne le même message qu'en entrée mais en ajoutant le status
          event.data.status = status
          return event.ports[0].postMessage(event.data)
        }).catch(error => {
          // On retourne le même message qu'en entrée mais en ajoutant l'erreur
          event.data.error = error
          return event.ports[0].postMessage(JSON.parse(JSON.stringify(event.data)))
        })
      } else if (event.data.type === 'clearCache') { // Dans le cas d'une demande de reset de cache
        // Vide le cache et retourne un Boolean pour la réponse
        return caches.delete(tileCacheName).then(response => {
          cacheKeys = []
          return event.ports[0].postMessage(response)
        }).catch(() => event.ports[0].postMessage(false))
      } else if (event.data.type === 'isReady') {
        event.ports[0].postMessage(true)
      } else if (event.data.type === 'getSizeCache') {
        // Permet de récupérer une taille approximative du cache
        return caches.open(tileCacheName).then(cache => cache.keys().then(keys => {
          return Promise.all(
            keys.map(req => cache.match(req).then(res => res.clone().blob().then(b => b.size)))
          ).then(sizes => event.ports[0].postMessage(sizes.reduce((acc, size) => acc + size, 0)))
        }))
      }
    }
  })
})()
