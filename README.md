# X7 third party dependencies

This repository holds all external fonts and javascript libraries that are outdated, but still in use in X7 progiciel.
We are placing these items here because they are not linked to the source code and should not be versioned in the same git repository.
These resources need to be managed and reduced as much as possible by using tools like npm, bower or the like.

The goal is to empty this repository step by step and replace these libraries with their current versions.